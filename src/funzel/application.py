#!/usr/bin/python3

#from emmi.scpi import MagicHuber

from caproto.asyncio.server import run as ca_run
from caproto.asyncio.server import start_server, Context

import logging, asyncio

import caproto as ca
import pyvisa
from parse import parse

from functools import partial, partialmethod

from emmi.scpi import MagicPharos

import funzel.flags as fflg

from funzel.ioc import PharosIoc

#from sys import args

# class ChannelData: ...

# class ChannelInteger(ChannelData):

#     # ...

#     def read():
#         data = ...
#         d2 = verify_value(data)
#         if d2 is not Skip...:
#             return d2
    
#     def write(): ...

#     def verify_value(): is integer...


# class FlorinsIntegerDaten:

#     def __init__(..., func): ...

#     def verify_value(...):

#         d3 = super().verify_value()
#         if d3 is not ...:
#             self.func(self, value)

#         return d3
           
        
    

# pvs = {
#     'name':  obj,  ## ca.ChannelData -> ChannelInt, ChannelFloat, ChannelByte
#     'name2': obj2  ## ca.ChannelData
# }


# class Ioc:

#     def __init__():

#         name = str()
#         obj = ...
#         self.pvs[name] = obj

#         name2 = str()
#         obj2 = ...
#         self.pvs[name2] = obj2
        

# class Context:
#     # ...

#     def run():
#         while True:
#             for name, obj in pvs,item():
#                 # ...beobachten, ob was reinkommt
#                 # gucken, ob es gerade fuer 'name' ist...
#                 # falls ja, dann sich die daten angucken,
#                 # ob sie zu OBJ passen, und ggf. hindurch schicken.


# def start_server():
    
#     ctx = Context(ioc.pvs)
#     ctx.run()



# class Moo(PVGroup):

#     foo = pvproperty()  ## <- KEIN ChannelData,
#     # PVSpec

#     @foo.putter
#     def func(): ...


# class PVSpec:
#     data = ChannelData...


#   .pvdb = { ... }



class Application:
    
    def __init__(self, prefix, args=None):
        if args is None:
            args = []

        self.prefix = prefix
        
        self.ioc = PharosIoc(
            self.prefix,
            #dev="TCPIP::192.168.136.217::1234::SOCKET",
            #dev="TCPIP::10.0.0.178::1234::SOCKET",
            dev="ASRL/dev/ttyUSB1::INSTR",
            rman="@py",
            #dev="ASRL1::INSTR",
            #rman="tests/visa-sim-huber.yml@sim",
        )

    async def async_run(self):
        
        logging.info(f'Starting IOC, PV list following')
        
        for pv in self.ioc.pvdb:
            logging.info(f"  {pv}")

        await start_server(self.ioc.pvdb)
        
        
def main():

    logging.basicConfig(level=logging.INFO)
    
    app = Application(prefix="KMC3:meeeh:")
    
    asyncio.run(app.async_run())

    print("exhub: Done")


if __name__ == "__main__":
    main()
