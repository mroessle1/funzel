#!/usr/bin/python3

from caproto.server import pvproperty, PVGroup
import logging, asyncio

import pyvisa
from parse import parse

from functools import partial, partialmethod

from emmi.scpi import MagicPharos

import funzel.flags as fflg

logger = logging.getLogger(__name__)

class PharosIoc(PVGroup):

    shutter_open = pvproperty(value=False)
    mains_state = pvproperty(value=False)
    chiller_temperature = pvproperty(value=21.0)
    chiller_temperature_RBV = pvproperty(value=0.0)
    
    def __init__(self, prefix, dev=None, rman=None, motors=None):

        self.pharos = self._init_device(dev, rman)
        self.prefix = prefix
        
        super().__init__(prefix)


    def _init_device(self, dev=None, rman=None):

        # useful for reconnecting on errors
        if dev is None:
            dev = self.param_dev
        else:
            self.param_dev = dev
        
        if rman is None:
            rman = self.param_rman
        else:
            self.param_rman = rman
        
        pharos = MagicPharos(device=dev, resource_manager=rman)
        helo = pharos.VERSION()

        try:
            assert "PHAROS UART module ver" in helo
            self.fsmc_version = tuple([int(i) for i in helo[23:].split('.')])
        except:
            raise RuntimeError(f'Cannot parse version from "{helo}"')
        
        logger.info(f'Pharos version: {self.fsmc_version}')
        
        return pharos
    

    async def status_query(self, dev):
        # Querries current status of the device
        # Returns a dictionary with flags/registers as defined in funzel.flags,
        # and their corresponding value.

        status = {}
        
        # Put the command in here (for commands which return orthogonal flags)
        ph_queries = (
            'MAINS_CTRL_STATE',
            'SHUTTER_STATE',
            'OSC_STATE',
            
            'MOTOR_CTRL_STATE',
            'OSC_SUPPLY_STATE',
            'RA_SUPPLY_STATE',
            'OSC_STATE',
            'CW_STATE',
            'RA_STATE',
            'HV1_CTRL_STATE',
            'HV2_CTRL_STATE',
            'SYNC_CTRL_STATE',
            'CHILLER_STATE',

            # 'OSC_OUT_STATE', ## Does this exist? (It's a "Timeout"...)
        )
        
        # Helper for command queries;
        # The idea is that every command has the same structure of reply, i.e.:
        #
        #   >>> CMD
        #   <<< DATA_TYPE: a b c d ...
        #
        # where CMD is the command, DATA_TYPE is a self-reported Pharos data structure
        # name (reproduced in funzel.flags), and a, b, c, d, ... are numerical values.
        # Reading commands is done therefore in a loop
        #
        # The main challenge is in the meaning of the numerical values (a, b, c...):
        #
        #   - flags-query commands: numbers are binary flags OR'ed together,
        #
        #   - values-query commands: numbers "plain" values (i.e. floating point
        #     numbers representing temperatures, counts, ...).
        #
        # The read-unpack loop is the same for all kinds of commands, but unpacking
        # is different for each of them. For the "values-query" numbers, we simply
        # associate each number (value) its corresponding key from funzel.flags.
        # For "flags-query" numbers, we wrap each number in a OrthogonalFlagsMap(),
        # allowing later easy access to the individual flags.
        #
        # The 'translator' lambdas to this.
        #
        # We decide which translator lambda to use for every value, depending
        # on how the corresponding funzel.flags.DATA_TYPE[field] is defined.

        ph_values_translator = lambda label, name, value: value

        ph_flags_translator = lambda vmap, name, value: \
            fflg.OrthogonalFlagsMap(vmap[name], code=value)

        for ph_cmd in ph_queries:

            ph_vals = dev.branch(ph_cmd).get()

            if not isinstance(ph_vals, tuple):
                logger.error(f'Unexpected reply for "{ph_cmd}": {ph_vals}')
                #raise RuntimeError(f'Unexpected reply for "{ph_cmd}": {ph_vals}')
                continue

            try:
                ph_type = ph_vals[-1]
                ph_map = getattr(fflg, ph_type)
            except AttributeError:
                logging.info(f"Can't unpack {ph_type}, data is: {ph_vals}")
                continue

            ph_flags = {}

            for k, v in zip(ph_map, ph_vals[:-1]):
                translator = ph_flags_translator if isinstance(ph_map[k], dict) \
                    else ph_values_translator
                ph_flags[k] = translator(ph_map, k, v)

            status[ph_type] = ph_flags

        self.log_pharos_status(status)
        print()

        return status


    async def pharos_write(self, cmd):

        # FIXME! Need to make this async!
        
        nr = self.pharos.kdev.write(cmd)
        tmp = len(cmd) + len(self.pharos.kdev.write_termination)
        
        if nr != tmp:
            raise RuntimeError(f'Error writing {cmd}: should have '+
                               f'been {tmp} bytes, were {nr}')
        
        ret = self.pharos.kdev.read()
        if ret != "Ok":
            raise RuntimeError(f'Command "{cmd}": received "{ret}"'+
                               f' instead of "Ok"')
                               
        logger.info(f'"{cmd}": "{ret}"')
        

    @chiller_temperature.putter
    async def chiller_temperature(self, inst, val):
        #v = int(val*100)
        v = val
        await self.pharos_write(f"CHILLER_SET_TEMP {v}")


    def log_pharos_status(self, status):
        for k,v in status.items():
            if isinstance(v, dict):
                for k2,v2 in v.items():
                    logger.info(f'  {k}.{k2}: {v2}')
            else:
                logger.info(f'{k}: {v}')


    @mains_state.scan(period=1.0)
    async def _update(self, inst, async_lib):
        status = await self.status_query(self.pharos)

        #await self.hello.write(status['hello'])
        await self.shutter_open.write(status['SHUTTER_CTRL']['STATE']['OPENED'])
        await self.chiller_temperature_RBV.write(status['CHILLER_CTRL']['WATER_TEMP'])
        
        #await self.chiller_temperature.write(status['CHILLER_CTRL']['SET_POINT'])

        #for pv_name,pv_obj in self.pvdb:
        #    if pv.find('auto_') == -1:
        #        continue
        #    await pv_obj.write(..'SHUTTER_CTRL_STATE_OPENED')        

        # update PVs...

        # Setter PVs
        # SYNC_SET_PP_ON
        # SYNC_SET_PP_OFF
        # SYNC_SET_INT_FREQUENCY
        # SYNC_SET_EXT_FREQUENCY

        # CHILLER_SET_TEMP
        # MOTOR_CTRL_MOVE_TO_POS
        # OSC_CTRL_RUN_STARTER
        # OSC_CTRL_SET_LOCKING
        # RA_CTRL_SET_LOCKING
        # OSC_SUPPLY_GET_WORK_TIME
        # OSC_SUPPLY_START
        # RA_SUPPLY_START
        # OSC_SUPPLY_STOP
        # RA_SUPPLY_STOP
        # OSC_SUPPLY_SET_I
        # RA_SUPPLY_SET_I
        # OSC_SUPPLY_SET_CLEAR_ERRORS
        # RA_SUPPLY_SET_CLEAR_ERRORS
        # MOTOR_CTRL_OPEN_SHUTTER
        # HV_PP_SET_ENABLE
        # HV_PP_SET_VOLTAGE

        

        
